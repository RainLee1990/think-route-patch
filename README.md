<!--
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-05-22 16:45:58
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-07-01 23:25:13
 * @Description: 文件说明
-->

# think-route-patch

#### 介绍

解决 ThinkPHP6 路由验证器优先于中间件执行的解决方案

#### 软件架构

使用 ThinkPHP6.0 的容器的功能绑定自定义的 `Route` 类库来解决路由验证器优先于路由中间件执行的问题

#### 安装教程

```shell
$ composer require rainlee/think-route-patch
```

#### 使用说明

修改 `app` 目录下 `provider.php` 文件，增加键为 `think\Route` 或 `route` 值为 `\rainlee\route\Route::class` 的参数

> 解决方案详细介绍：https://blog.csdn.net/lihongyu101/article/details/124925559
