<?php
/*
 * @Author: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @Date: 2022-05-21 21:00:26
 * @LastEditors: 李红雨 - RainLee <rainlee1990@yeah.net>
 * @LastEditTime: 2022-05-22 16:55:26
 * @Description: 文件说明
 */

namespace rainlee\route;

use think\Request;
use rainlee\route\dispatch\Callback as CallbackDispatch;
use rainlee\route\dispatch\Controller as ControllerDispatch;

class RuleItem extends \think\route\RuleItem
{
    /**
     * 解析URL地址为 模块/控制器/操作
     * @access protected
     * @param  Request $request Request对象
     * @param  string  $route 路由地址
     * @return CallbackDispatch
     */
    protected function dispatchMethod(Request $request, string $route): CallbackDispatch
    {
        $path = $this->parseUrlPath($route);

        $route  = str_replace('/', '@', implode('/', $path));
        $method = strpos($route, '@') ? explode('@', $route) : $route;

        return new CallbackDispatch($request, $this, $method, $this->vars);
    }

    /**
     * 解析URL地址为 模块/控制器/操作
     * @access protected
     * @param  Request $request Request对象
     * @param  string  $route 路由地址
     * @return ControllerDispatch
     */
    protected function dispatchController(Request $request, string $route): ControllerDispatch
    {
        $path = $this->parseUrlPath($route);

        $action     = array_pop($path);
        $controller = !empty($path) ? array_pop($path) : null;

        // 路由到模块/控制器/操作
        return new ControllerDispatch($request, $this, [$controller, $action], $this->vars);
    }
}
